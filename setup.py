from setuptools import setup, find_packages

setup(
    name='imPy',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'imPy',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)
