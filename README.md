# Qt Image Processing Tools in Python

A collection of image processing tools with a handy Qt user interface.

## Developing

It is recommended to work with a `virtualenv` for this repository. The basics for
that are:

```bash
git clone git@gitlab.com:tdpu/imPy.git
cd imPy
virtualenv -p python3 myEnv
source myEnv/bin/activate
pip install -r requirements.txt
# develop stuff
deactivate
```
