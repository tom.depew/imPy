import QtQuick 2.1
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0

ColumnLayout {
    Label {
        text: "Example"
    }
    Slider {
        value: 0.5
    }
    Button {
        text: "Press ME"
    }
}

