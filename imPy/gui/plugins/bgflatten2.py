"""
bgremove plugin widget controls
"""
import numpy as np
from PyQt5 import uic, QtCore
from core.filter.bgflatten import bgflatten
from gui.plugins.PluginWidget import PluginWidget

class BGFlattenPlugin(PluginWidget):
    """ BGFlattenPlugin Widget controls and PIPE interface """
    def __init__(self, parent=None):
        PluginWidget.__init__(self, parent=None)
        self.parent = parent

        # set up UI
        self.ui = uic.loadUi('gui/plugins/bgflatten.ui')

        # find place in filter chain
        self.filterID = self.parent.currentFilter
        self.parent.currentFilter += 1

        # reference input image
        self.input = np.zeros((10, 10))
        self.output = np.zeros((10, 10))

        # bgflatten default parameters
        self.sigma = 3.0

        # set ui element values
        self.ui.bgLineEdit.setText(str(self.sigma))
        self.ui.bgSlider.setValue(self.sigma)

        # set signal connections
        self.ui.bgCheckBox.clicked.connect(self.BGtoggle)
        self.ui.bgLineEdit.editingFinished.connect(self.BGedit)
        self.ui.bgSlider.valueChanged.connect(self.BGslider)

    def filter(self):
        """ apply filter/update function """
    	# if desired, execute the filter on input, set the output
        if self.ui.bgCheckBox.isChecked() is True:
            self.output = bgflatten(self.input, self.sigma)
        else:
            self.output = self.input

        # update the main canvas, notify of location in filter chain
        self.parent.doUpdate(self.filterID)

    def BGtoggle(self):
        # toggle On/Off?
        self.filter()

    def BGedit(self):
        # check if the value has changed
        self.sigma = np.float(self.ui.bgLineEdit.text())

        # set the slider value to match
        self.ui.bgSlider.setValue(self.sigma)

        # update the operation
        self.BGtoggle()

    def BGslider(self):
        # check if value has changed
        self.sigma = self.ui.bgSlider.value()

        # set the line edit's value to match
        self.ui.bgLineEdit.setText(str(self.sigma))

        # update the bg operation
        self.BGtoggle()
