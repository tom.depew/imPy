"""
bgremove plugin widget
"""
import numpy as np
from PyQt5 import uic, QtCore
from core.filter.bgRemove import bgRemove
from gui.plugins.PluginWidget import PluginWidget

class BGRemovePlugin(PluginWidget):
    def __init__(self, parent=None):
        PluginWidget.__init__(self, parent=None)
        # self.setParent(parent)
        self.parent = parent

        # set up UI
        self.ui = uic.loadUi('gui/plugins/bgremove.ui')

        # find place in filter chain
        self.filterID = self.parent.currentFilter
        self.parent.currentFilter += 1

        # reference input image
        self.input = self.parent.imgs[self.filterID]
        self.output = self.parent.imgs[self.filterID]

        # updated signal
        self.updated = QtCore.pyqtSignal()

        # BG Remove specific parameters
        self.sigma = 3.0

        self.ui.bgLineEdit.setText(str(self.sigma))
        self.ui.bgSlider.setValue(self.sigma)

        # set signal connections
        self.ui.bgCheckBox.clicked.connect(self.BGtoggle)
        self.ui.bgLineEdit.editingFinished.connect(self.BGedit)
        self.ui.bgSlider.valueChanged.connect(self.BGslider)

    def filter(self, imgIn, imgOut):
	    # execute the filter on input, set the output
        imgOut = bgRemove(imgIn, self.sigma)

    def BGtoggle(self):
        # toggle On/Off?
        if (self.ui.bgCheckBox.isChecked() == True):
            # determine the filter width
            self.sigma = np.float(self.ui.bgLineEdit.text())

            # apply the filter
            # self.filter()
            self.parent.imgs[self.filterID+1] = bgRemove(self.parent.imgs[self.filterID], self.sigma)
            # self.updated.emit()

            # self.output = bgRemove(self.input, self.sigma)

        else:
            # remove the bg correction
            # self.output = self.input
            self.parent.imgs[self.filterID+1] = self.parent.imgs[self.filterID]

        # update Processed Image canvas
        # self.parent.canvas1.draw(self.parent.img1)
        self.parent.canvas1.draw(self.parent.imgs[self.filterID+1])

    def BGedit(self):
        # check if the value has changed
        sigma = np.float(self.ui.bgLineEdit.text())

        # set the slider value to match
        self.ui.bgSlider.setValue(sigma)

        # update the operation
        self.BGtoggle()

    def BGslider(self):
        # check if value has changed
        sigma = self.ui.bgSlider.value()

        # set the line edit's value to match
        self.ui.bgLineEdit.setText(str(sigma))

        # update the bg operation
        self.BGtoggle()
