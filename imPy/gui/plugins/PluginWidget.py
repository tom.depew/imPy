#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
from PyQt5 import QtCore
from PyQt5 import QtWidgets

class ScrollEdit(QtWidgets.QWidget):
    """ A scroll bar + line edit combination widget """
    def __init__(self, initValue=1, minValue=1, maxValue=100):
        super().__init__()

        # consists of a QHBoxLayout containing a scroll bar and line edit widgets
        self.hbox = QtWidgets.QHBoxLayout()
        self.lineEdit = QtWidgets.QLineEdit(self)
        self.scrollBar = QtWidgets.QScrollBar(QtCore.Qt.Horizontal, self)

        self.scrollBar.setMaximum(maxValue)
        self.scrollBar.setMinimum(minValue)
        self.scrollBar.setValue(initValue)
        self.lineEdit.setText(str(initValue))

        self.hbox.addWidget(self.lineEdit, 1)
        self.hbox.addWidget(self.scrollBar, 4)

        self.setLayout(self.hbox)

		# initialize connections
        self.scrollBar.valueChanged.connect(self.sliderChanged)
        self.lineEdit.returnPressed.connect(self.editChanged)

	# slot functions    
    def sliderChanged(self, value):
        self.lineEdit.setText(str(value))

    def editChanged(self):
        value = int(self.lineEdit.text())
        self.scrollBar.setValue(value)

class HeaderBar(QtWidgets.QWidget):
    """ Plugin Widget Header """
    def __init__(self, parent=None, title='Random Plugin'):
        super().__init__()
        self.parent = parent

        # the header box is contained in a QHBoxLayout
        self.headBox = QtWidgets.QHBoxLayout()

        # header label (the plugin name)
        self.headLabel = QtWidgets.QLabel(self)
        self.headLabel.setText(title)

        # header button to expand/collapse
        self.headButton = QtWidgets.QPushButton(self)
        self.headButton.setFlat(True)
        self.headButton.setText("Hide")

        # insert widgets into layout
        self.headBox.addWidget(self.headLabel, 4)
        self.headBox.addWidget(self.headButton, 1)

        self.setLayout(self.headBox)

        # initial state is NOT collapsed
        self.collapsed = False

        # initialize connections
        self.headButton.clicked.connect(self.collapse)

    def collapse(self):
        if self.collapsed is True:
            self.parent.body.setVisible(True)
            self.headButton.setText("Hide")
            self.collapsed = False
        else:
            self.parent.body.setVisible(False)
            self.headButton.setText("Show")
            self.collapsed = True

class PluginWidget(QtWidgets.QWidget):
    """ Plugin Widget class for qtPipe """
    def __init__(self, parent=None, pluginName='Plugin Name'):
        super().__init__()
        self.pluginName = pluginName

        self.filterID = None
        self.input = None
        self.output = None

        self.initUI() 
 
    def initUI(self):
        # a vbox layout contains all elements of the plugin
        self.pluginBox = QtWidgets.QVBoxLayout()

        # add the header to the plugin layout
        self.header = HeaderBar(self, self.pluginName)
        self.pluginBox.addWidget(self.header)

        # the rest of the plugin content is then added
        self.body = QtWidgets.QWidget(self)
        self.bodyBox = QtWidgets.QVBoxLayout()
        self.body.setLayout(self.bodyBox)
        self.body.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding, QtWidgets.QSizePolicy.MinimumExpanding)

        # plugin controls
        self.tog = QtWidgets.QCheckBox(self)
        self.tog.setText("Toggle")

        self.bodyBox.addWidget(self.tog)

        self.scrollEdit = ScrollEdit()
        self.bodyBox.addWidget(self.scrollEdit)

        self.pluginBox.addWidget(self.body)
        
        # add stretch to end of layout, helps with collapse
        self.pluginBox.addStretch(1)

        self.setLayout(self.pluginBox)
        
        #self.setWindowTitle('qtPipe Plugin Widget')
        #self.show()
