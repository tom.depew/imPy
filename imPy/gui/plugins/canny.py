"""
canny plugin widget controls
"""
import numpy as np
from PyQt5 import uic
from gui.plugins.PluginWidget import PluginWidget
from core.feature.canny import canny

class CannyPlugin(PluginWidget):
    """ CannyPlugin Widget controls and PIPE interface """
    def __init__(self, parent=None):
        PluginWidget.__init__(self, parent=None)
        # self.setParent(parent)
        self.parent = parent

        # set up UI
        self.ui = uic.loadUi('gui/plugins/canny.ui')

        # find place in filter chain
        self.filterID = self.parent.currentFilter
        self.parent.currentFilter += 1

        # reference input image
        self.input = np.zeros((10, 10))
        self.output = np.zeros((10, 10))

        # canny default parameters
        self.thresholdHigh = 150.0
        self.thresholdLow = 50.0
        self.apertureSize = 3

        # set ui element values
        self.ui.highLineEdit.setText(str(self.thresholdHigh))
        self.ui.highScrollBar.setValue(self.thresholdHigh)
        self.ui.highScrollBar.setMaximum(255)
        self.ui.highScrollBar.setMinimum(1)

        self.ui.lowLineEdit.setText(str(self.thresholdLow))
        self.ui.lowScrollBar.setValue(self.thresholdLow)
        self.ui.lowScrollBar.setMaximum(255)
        self.ui.lowScrollBar.setMinimum(1)

        self.ui.sobelScrollBar.setValue(self.apertureSize)
        self.ui.sobelLineEdit.setText(str(self.apertureSize))
        self.ui.sobelScrollBar.setMaximum(7)
        self.ui.sobelScrollBar.setMinimum(3)

        # set signal connections
        self.ui.cannyCheckBox.clicked.connect(self.cannyToggle)
        self.ui.lowLineEdit.editingFinished.connect(self.lowEdit)
        self.ui.lowScrollBar.valueChanged.connect(self.lowScroll)
        self.ui.highLineEdit.editingFinished.connect(self.highEdit)
        self.ui.highScrollBar.valueChanged.connect(self.highScroll)
        self.ui.sobelLineEdit.editingFinished.connect(self.sobelEdit)
        self.ui.sobelScrollBar.valueChanged.connect(self.sobelScroll)

    def filter(self):
        # execute the filter on input, set the output
        if self.ui.cannyCheckBox.isChecked() is True:
            # stupid thing
            sobelvals = np.uint([3,5,7])

            blah = sobelvals[np.argmin(np.abs(self.apertureSize-np.array([3,5,7])))]
            self.output = canny(self.input, blah, self.thresholdLow, self.thresholdHigh)
        else:
            self.output = self.input

        # update the main canvas, notify of location in filter chain
        self.parent.doUpdate(self.filterID)

    def cannyToggle(self):
        # toggle On/Off?
        self.filter()

    def sobelEdit(self):
        # check if the value has changed
        self.apertureSize = np.float(self.ui.sobelLineEdit.text())

        # set the slider value to match
        self.ui.sobelScrollBar.setValue(self.apertureSize)

        # update the operation
        self.cannyToggle()

    def sobelScroll(self):
        # check if value has changed
        self.apertureSize = self.ui.sobelScrollBar.value()

        # set the line edit's value to match
        self.ui.sobelLineEdit.setText(str(self.apertureSize))

        # update the bg operation
        self.cannyToggle()

    def lowEdit(self):
        # check if the value has changed
        self.thresholdLow = np.float(self.ui.lowLineEdit.text())

        # set the slider value to match
        self.ui.lowScrollBar.setValue(self.thresholdLow)

        # update the operation
        self.cannyToggle()

    def lowScroll(self):
        # check if value has changed
        self.thresholdLow = self.ui.lowScrollBar.value()

        # set the line edit's value to match
        self.ui.lowLineEdit.setText(str(self.thresholdLow))

        # update the bg operation
        self.cannyToggle()

    def highEdit(self):
        # check if the value has changed
        self.thresholdHigh = np.float(self.ui.highLineEdit.text())

        # set the slider value to match
        self.ui.highScrollBar.setValue(self.thresholdHigh)

        # update the operation
        self.cannyToggle()

    def highScroll(self):
        # check if value has changed
        self.thresholdHigh = self.ui.highScrollBar.value()

        # set the line edit's value to match
        self.ui.highLineEdit.setText(str(self.thresholdHigh))

        # update the bg operation
        self.cannyToggle()
