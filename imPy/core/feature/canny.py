def canny(image, aperture = 3, thresh1 = 10, thresh2 = 99, L2 = True):
    import numpy as np
    import cv2
    """ PIPE wrapper for the cv2.Canny function.

    Parameters
    ----------
    image : np.array([uint8])
        The input image
    aperture : int
        Defines the size of the Sobel operator for smoothing
    thresh1 : float
        Lower hysteresis threshold
    thresh2 : float
        Upper hysteresis threshold
    L2 : bool
        True - use L2 norm for image gradient magnitude
        False - use L1 norm for image gradient magnitude

    Returns
    -------
    np.array([])
        The identified edge image (binary)

    Notes
    -----
    The Gaussian kernel is defined in Fourier space as

    .. math::  L2(x, y) = \sqrt{ \left( \frac{dI}{dx} \right)^2 + \left( \frac{dI}{dy} \right)^2  }
    .. math::  L1(x, y) = \left| \frac{dI}{dx} \right| + \left| \frac{dI}{dy} \right|

    Example
    -------
    >>> canny(image, aperture = 10, thresh1 = 10, thresh2 = 99, L2 = True)

    See Also
    --------
    cv2.Canny

    Literature
    ----------
    John Canny. A computational approach to edge detection. Pattern Analysis and Machine Intelligence,
    IEEE Transactions on, (6):679–698, 1986.
    """

    return cv2.Canny(np.uint8(image), threshold1=thresh1, threshold2=thresh2, apertureSize=aperture)
