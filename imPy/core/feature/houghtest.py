# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import sys
import cv2
import skimage
import matplotlib.pyplot as plt

sys.path.append("/home/tom/work/dev/imPy/imPy/")
from core.filter.bgRemove import bgRemove

# close all figures
plt.close('all')

image = cv2.imread("/home/tom/work/dev/imPy/imPy/sample-images/3002-3_borescope.bmp")

# flatten
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

image[234:,:85] = 0

plt.figure()
plt.imshow(image, cmap='gray')
plt.show()

# bg correct
image = bgRemove(image)

plt.figure()
plt.imshow(image, cmap='gray')
plt.show()