# import modules

def highpass(image, sigma = 10):
    """ 2D High Pass filter.

    Apply a Gaussian High Pass Filter 2D Fourier space.  This will emphasize sharp edges and enhance noise.

    Parameters
    ----------
    image : np.array([])
        The input image
    sigma : float
        Fourier Gaussian Filter width (FWHM)

    Returns
    -------
    np.array([])
        The filtered image

    Notes
    -----
    The Gaussian kernel is defined in Fourier space as

    .. math:: w(m, n) = 1 - e^{ -\frac{1}{2 \sigma^2}\left(m^2 + n^2\right) }

    Example
    -------
    >>> highpass(image, sigma=10)
    """

    # requires numpy
    import numpy as np

    # setup meshgrid
    if len(image.shape) == 2:
        Ny, Nx = image.shape
    elif len(image.shape) == 3:
        Ny, Nx, BITDEPTH = image.shape

    # compute the center
    center = np.array([Ny,Nx])/2
    # construct a meshgrid and create Gaussian Low-Pass Filter
    xx, yy = np.meshgrid(np.arange(0,Nx), np.arange(0,Ny))

    # Gaussian Filter with width sigma
    LPF = 1 - np.exp(-(xx-center[1])**2/(2*sigma**2)) * np.exp(-(yy-center[0])**2/(2*sigma**2))

    # Transform to Fourier space
    f = np.fft.fftshift(np.fft.fft2(image))

    # High pass filter
    fHPF = f*HPF

    # Transform back to Image space
    HPimage = np.fft.ifft(np.fft.fftshift(fHPF))

    # return scaled to 0-255
    return 255*HPimage/HPimage.max()
