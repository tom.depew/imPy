def bgflatten(image, sigma = 3):
    import numpy as np
    ''' Attempt to remove slow varying background intensity modulation using
    a low pass filter in Fourier space.  Useful to remove slow varying background
    intensity modulation.

    Parameters
    ----------
    image : np.array([])
        The input image
    sigma : float
        Fourier Gaussian Filter width (FWHM)

    Returns
    -------
    np.array([])
        The filtered image

    Notes
    -----
    The Gaussian kernel is defined in Fourier space as

    .. math::  w(m, n) = e^{ -\frac{1}{2 \sigma^2}\left(m^2 + n^2\right) }

    Example
    -------
    >>> bgflatten(image, sigma=10)
    '''

    # setup meshgrid
    if len(image.shape) == 2:
        Ny, Nx = image.shape
    elif len(image.shape) == 3:
        Ny, Nx, BITDEPTH = image.shape

    # compute the center
    center = np.array([Ny,Nx])/2
    # construct a meshgrid and create Gaussian Low-Pass Filter
    xx, yy = np.meshgrid(np.arange(0,Nx), np.arange(0,Ny))

    # Gaussian Filter with width sigma
    LPF = np.exp(-(xx-center[1])**2/(2*sigma**2)) * np.exp(-(yy-center[0])**2/(2*sigma**2))

    # Transform to Fourier space
    f = np.fft.fftshift(np.fft.fft2(image))

    # Low pass filter
    fLPF = f*LPF

    # demodulate image
    fLPF = image/abs(np.fft.ifft2(np.fft.fftshift(fLPF)))
    # return scaled to 0-255
    return np.uint8(255*fLPF/fLPF.max())

