#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 12 15:10:40 2017

@author: tdepe
"""

import sys

# GUI stuff
from PyQt5 import QtCore, QtGui, QtWidgets, uic, QtQuick
#from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
#from matplotlib.figure import Figure
#from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

# other imports
import cv2
import numpy as np
from OpenGL import GL

sys.path.append('/home/tdepe/work/pipe/')

from gui.plugins.bgflatten import BGFlattenPlugin
from gui.plugins.canny import CannyPlugin

MainWindowUI, MainWindowBase = uic.loadUiType('gui/mainUI/mainui.ui')

# Custom Matplotlib Canvas Widget
#class mplCanvas(FigureCanvas):
#    # add an mplCanvas widget to the canvas
#    #self.canvasX = mplCanvas(self.centralWidget, width=5, height=5, dpi=100)
#    #self.imgLayoutX.addWidget(self.canvasX)
#    def __init__(self, parent=None, width=5, height=4, dpi=100):
#        fig = Figure(figsize=(width, height), dpi=dpi)
#        self.axes = fig.add_subplot(111)
#
#        self.initialize()
#
#        FigureCanvas.__init__(self, fig)
#        self.setParent(parent)
#
#        FigureCanvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
#        FigureCanvas.updateGeometry(self)
#
#    def initialize(self):
#        pass
#
#    def drawstuff(self, image):
#        self.axes.imshow(image, cmap='gray', interpolation='none')
#        self.draw()

class ImgCanvas(QtWidgets.QLabel):
    """ Class to provide a basic image canvas """
    def __init__(self, parent=None, width=5, height=4):

        QtWidgets.QLabel.__init__(self)
        self.setParent(parent)
        self._pixmap = QtGui.QPixmap(self.pixmap())

        # set canvas size policy and minimum size
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        self.setMinimumSize(100, 100)

    def draw(self, image):
        # get a QImage and convert to scaled QPixmap
        #img = QtGui.QImage(image, image.shape[1], image.shape[0], QtGui.QImage.Format_RGB888)
        img = QtGui.QImage(image, image.shape[1], image.shape[0], QtGui.QImage.Format_Grayscale8)
        pmap = QtGui.QPixmap.fromImage(img)

        # set the ImgCanvas._pixmap to the desired image
        self._pixmap = pmap
        self.setPixmap(self._pixmap.scaledToWidth(self.width()))

    def resizeEvent(self, event):
        if self._pixmap.isNull() is False:
            self.setPixmap(self._pixmap.scaledToWidth(self.width()))

# Main Application window
class MainWindow(MainWindowBase, MainWindowUI):
    """ Main Window class for qtPIPE """
    def __init__(self, parent=None):
        MainWindowBase.__init__(self, parent)
        self.setupUi(self)
        self.setWindowIcon(QtGui.QIcon("/home/tdepe/work/qtpipe/qtpipe/gui/mainUI/images/coanda_logo.png"))

        # try QImage widgets for better performance
        self.canvas0 = ImgCanvas()
        self.imgLayout1.addWidget(self.canvas0)
        self.canvas1 = ImgCanvas()
        self.imgLayout2.addWidget(self.canvas1)

        # initialize starting filter ID number and filter list
        self.currentFilter = 0
        self.filterList = []

        # initialize the images to nothing
        self.img0 = np.zeros((10, 10))
        self.img1 = np.zeros((10, 10))

        # update the graphics canvas
        self.canvas0.draw(self.img0)
        self.canvas1.draw(self.img1)

        # Connections/Sockets
        # image loading
        self.actionOpen.triggered.connect(self.clickLoadImage)
        self.actionExit.triggered.connect(self.close)

        #self.loadPushButton.clicked.connect(self.clickLoadImage)
        # set vertical size policy for plugin area
        #self.pluginLayout.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)

        # BG Removal Frame
        self.filterList.append(BGFlattenPlugin(self))
        self.pluginLayout.addWidget(self.filterList[0])

        self.filterList.append(BGFlattenPlugin(self))
        self.pluginLayout.addWidget(self.filterList[1])
        # Canny Edge Detector Frame
        #self.filterList.append(CannyPlugin(self))
        #self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.filterList[1].ui)

        # try to insert a qml view
        #view = QtQuick.QQuickView()
        #view.setSource(QtCore.QUrl('test.qml'))

        #container = QtWidgets.QWidget.createWindowContainer(view, self)
        #self.pluginLayout.addWidget(container)

        # add stretch to facilitate collapsing
        self.pluginLayout.addStretch(1)

    def clickLoadImage(self):
        """ Open a File Dialog to allow user to select image to load """
        # request a file from the user with QFileDialog
        imFileName, _ = QtWidgets.QFileDialog.getOpenFileName(self.centralWidget, 'Choose an Image File to Load')

        # read the image file and update the source image label
        #img = cv2.imread(imFileName)
        self.label1.setText('Source Image: ' + imFileName)

        self.img0 = cv2.cvtColor(cv2.imread(imFileName), cv2.COLOR_BGR2GRAY)
        self.img1 = self.img0

        # set the input of the first filter
        self.filterList[0].input = self.img0
        self.filterList[0].filter()

        # update the graphics canvas
        self.canvas0.draw(self.img0)

        # notify user via statusbar
        self.statusBar.showMessage("Image Loaded.", 2000)

    def doUpdate(self, idx):
        """ Update the filter chain """
        # called when any filter class has a parameter change
        # if there is a subsequent filter, update the result:
        if idx+1 < len(self.filterList):
            self.filterList[idx+1].input = self.filterList[idx].output
            self.filterList[idx+1].filter()
        else:
            # reached the end, we may update and draw the canvas
            self.img1 = self.filterList[idx].output
            self.canvas1.draw(self.img1)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
#    pixmap = QtGui.QPixmap(os.path.join(CURR, "splash.png"))
#    splash = QtGui.QSplashScreen(pixmap)
#    splash.show()
#    app.processEvents()

    mainWin = MainWindow(None)
#    center(MainWindow)
#    icon(MainWindow)
    mainWin.show()
#    splash.finish(MainWindow)
    app.exec_()
